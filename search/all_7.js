var searchData=
[
  ['get_0',['get',['../classshares_1_1_queue.html#a45835daf8ee60391cca9667a942ade25',1,'shares::Queue']]],
  ['get_5fdelta_1',['get_delta',['../classencoder_1_1_encoder.html#a2f451b6cb3e85e03d45e0ac097e29a29',1,'encoder::Encoder']]],
  ['get_5fposition_2',['get_position',['../classencoder_1_1_encoder.html#abc44b0bb3d2ee93571f00d6bab5e7c53',1,'encoder::Encoder']]],
  ['getcalcoefficients_3',['getCalCoefficients',['../class_i2_c_1_1_b_n_o055.html#ac76ded69afcbb14a019da17e8825a6ff',1,'I2C.BNO055.getCalCoefficients()'],['../classtouch_1_1_touch.html#a75f00c02931286eee362f9fbb179d522',1,'touch.Touch.getCalCoefficients()']]],
  ['getcalstatus_4',['getCalStatus',['../class_i2_c_1_1_b_n_o055.html#a2c8ba73086775e8300aa6e1f9c6fa69d',1,'I2C::BNO055']]],
  ['geteulerangles_5',['getEulerAngles',['../class_i2_c_1_1_b_n_o055.html#a65c81e639002e8a3887779861cc273f0',1,'I2C::BNO055']]],
  ['getvelocity_6',['getVelocity',['../class_i2_c_1_1_b_n_o055.html#adb48ffdd8ab26adb69cec1293ab59f36',1,'I2C::BNO055']]]
];
